import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { ScrollView, View, Text } from 'react-native'

export class SchoolScreen extends Component {

  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: 'Escolas',
  };

  componentWillMount() {
  }

  render () {

    return (
      <View>
        <ScrollView>
          <View>
            <Text>Welcome!</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { 
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(null, null)(SchoolScreen);