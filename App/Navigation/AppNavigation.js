import { StackNavigator, TabNavigator } from 'react-navigation'
import SchoolsScreen from '../Containers/SchoolsScreen'

const PrimaryNav = StackNavigator({
  SchoolsScreen: { screen: SchoolsScreen }
}, {
  // Default config for all screens
  initialRouteName: 'SchoolsScreen',
})

export default PrimaryNav
