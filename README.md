# README #

Projeto base para criação de aplicações em react native

### O que vem junto? ###

* https://github.com/reactjs/redux
* https://github.com/reactjs/react-redux
* https://github.com/redux-saga/redux-saga
* https://github.com/react-community/react-navigation
* https://github.com/rtfeldman/seamless-immutable


### Como começar? ###

> yarn install

> react-native run-android